@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

        	<div class="panel panel-default">
                <div class="panel-body">

                	<div class="page-header">
					  <h1>{{ $user->name }} <small>joined {{ $user->created_at->diffForHumans() }}</small></h1>
					</div>

					@if(!Auth::check())
						@if(\App\Follow::following(Auth::id(), $user->id))
							<form style="display: inline;" method="post" action="{{ route('unfollow') }}">
								{{ csrf_field() }}
								<input type="hidden" name="id" value="{{ $user->id }}">
								<button class="btn btn-lg btn-danger">Unfollow</button>
							</form>
						@else
							<form style="display: inline;" method="post" action="{{ route('follow') }}">
								{{ csrf_field() }}
								<input type="hidden" name="id" value="{{ $user->id }}">
								<button class="btn btn-lg btn-primary">Follow</button>
							</form>
						@endif
					@endif

                </div>
        	</div>

        	@foreach($user->quotes as $quote)
	            <div class="panel panel-default">
	            	<div class="panel-heading">{{ $user->name }}'s quotes</div>
	                <div class="panel-body">
	                    <div class="row">
	                        <div class="col-md-12">
	                            @if(Auth::id() == $quote->user->id)
	                            	<div class="btn-group pull-right" role="group">
		                                <button type="button" class="btn btn-link btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		                                  <span class="caret"></span>
		                                </button>
		                                <ul class="dropdown-menu">
		                                  <li><a href="#" onclick="event.preventDefault(); document.getElementById('delete-form{{ $quote->id }}').submit();">Delete</a></li>
		                                  <form id="delete-form{{ $quote->id }}" action="{{ route('delete-quote') }}" method="POST" style="display: none;">
		                                    <input type="hidden" name="id" value="{{ $quote->id }}">
		                                    {{ csrf_field() }}
		                                </form>
		                                  <li><a href="#">Make this private</a></li>
		                                </ul>
		                            </div>
	                            @endif
	                        </div>
	                        <div class="col-md-2">
	                            <a href="{{ route('user', $quote->user->id) }}" class="thumbnail">
	                              <img src="http://www.freeiconspng.com/uploads/face-avatar-png-14.png" alt="avatar" class="img-responsive">
	                            </a>
	                        </div>
	                        <div class="col-md-10">
	                            <p style="margin-bottom: 0;"><strong><a href="{{ route('user', $quote->user->id) }}">{{ $quote->user->name }}</a></strong></p>
	                            <small>{{ $quote->created_at->diffForHumans() }}</small>
	                            <h5>{{ $quote->quotes }}</h5>
	                        </div>
	                    </div>
	                    
	                </div>
	                <div class="panel-footer clearfix">
	                    <span class="pull-left"></span>
	                    <span class="pull-right">
	                        <form method="post" action="{{ route('like-quote') }}">
	                            {{ csrf_field() }}
	                            <input type="hidden" name="id" value="{{ $quote->id }}">
	                            <button class="btn btn-xs btn-primary" {{ ( \App\Like::liked(Auth::id(), $quote->id) ? 'disabled' : '' ) }}>{{ ( $quote->has('likes') ? $quote->likes->count() : '' ) }} <span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span></button>
	                        </form>
	                    </span>
	                 </div>
	            </div>
	        @endforeach

        </div>
    </div>
</div>
@endsection
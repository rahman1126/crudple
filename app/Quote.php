<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    protected $table = 'quotes';
    protected $fillable = ['user_id', 'quotes', 'slug', 'private'];

    // each quotes belongs to a user
    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    // each quotes has many likes
    public function likes()
    {
    	return $this->hasMany('App\Like');
    }

}

<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    // each user has many quotes
    public function quotes()
    {
        return $this->hasMany('App\Quote');
    }


    public static function followers()
    {
        return $this->belongsToMany('App\User', 'follows', 'following_id', 'follower_id')->withTimestamps();
    }

    // Get all users we are following
    public function following()
    {
        return $this->belongsToMany('App\User', 'follows', 'follower_id', 'following_id')->withTimestamps();
    }

    // get feed
    public static function getFeed(User $user) 
    {
        $userIds = $user->following()->pluck('following_id');
        // dd($userIds);
        $userIds[] = $user->id;
        return \App\Quote::whereIn('user_id', $userIds)->latest()->paginate(10);
    }

    // get followers
    public static function getFollowing(User $user)
    {
        $userIds = $user->following()->pluck('following_id');
        return User::whereIn('id', $userIds)->latest()->paginate(5);
    }

    // get new people

}

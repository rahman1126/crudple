<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::prefix('/home')->group(function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/user/{id}', 'HomeController@user')->name('user');

    // quotes
    Route::post('/quote/create', 'QuoteController@create')->name('create-quote');
    Route::post('/quote/delete', 'QuoteController@delete')->name('delete-quote');
    Route::post('/quote/like', 'QuoteController@like')->name('like-quote');

    // user
    Route::post('/user/follow', 'UserController@follow')->name('follow');
    Route::post('/user/unfollow', 'UserController@unfollow')->name('unfollow');

});



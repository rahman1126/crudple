<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    protected $table = 'follows';
    protected $fillable = ['follower_id', 'following_id'];

    public static function following($user_id, $following_id)
    {
    	$following = Follow::where('follower_id', $user_id)->where('following_id', $following_id)->first();

    	if ($following != null) {
    		return true;
    	} else {
    		return false;
    	}
    }

}

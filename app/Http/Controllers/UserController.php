<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\Quote;
use App\User;
use App\Like;
use App\Follow;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function follow(Request $request)
    {

    	$follow = new Follow;
    	$follow->follower_id = Auth::id();
    	$follow->following_id = $request->input('id');
    	$follow->save();

    	return redirect()->back();
    	
    }

    public function unfollow(Request $request)
    {

    	$follow = Follow::where('follower_id', Auth::id())->where('following_id', $request->input('id'))->first();
    	$follow->delete();

    	return redirect()->back();
    	
    }


}

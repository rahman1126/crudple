<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $table = 'likes';
    protected $fillable = ['user_id', 'quote_id'];

    public static function liked($user_id, $quote_id)
    {
    	$liked = Like::where('user_id', $user_id)->where('quote_id', $quote_id)->first();

    	if ($liked != null) {
    		return true;
    	} else {
    		return false;
    	}
    }
}

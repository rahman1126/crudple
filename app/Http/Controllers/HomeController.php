<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\Quote;
use App\User;
use App\Like;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $peoples = User::latest()->limit(5)->get();
        $following = User::getFollowing(Auth::user());
        $quotes = User::getFeed(Auth::user());
        return view('home')->with('quotes', $quotes)
            ->with('peoples', $peoples)
            ->with('following', $following);
    }

    // user page
    public function user($id)
    {
        $user = User::find($id);
        return view('user')->with('user', $user);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use Auth;

use App\Quote;
use App\Like;

class QuoteController extends Controller
{
    function __construct()
    {
    	$this->middleware('auth');
    }

    // create new quote page
    public function create(Request $request)
    {
    	$valid = Validator::make($request->all(), [
    		'quote'		=> 'required|max:255',
    	]);

    	if ($valid->fails()) {
    		return redirect()->back()
    			->withInput()
    			->withErrors($valid);
    	} else {

    		$quote = new Quote;
    		$quote->user_id = Auth::id();
    		$quote->quotes = $request->input('quote');
    		$quote->slug = str_limit($request->input('quote'), 80) . '-' . time();
    		$quote->private = ( $request->input('private') == '' ? 0 : 1 );
    		$quote->save();

    		return redirect()->back();

    	}
    }

    // delete
    public function delete(Request $request)
    {
    	$quote = Quote::find($request->input('id'));
    	$quote->delete();

    	return redirect()->back();
    }

    // like
    public function like(Request $request)
    {

    	if ($this->check_like($request->input('id')) == true) {
    		$likes = new Like;
	    	$likes->user_id = Auth::id();
	    	$likes->quote_id = $request->input('id');
	    	$likes->save();
	    	
    	}

    	return redirect()->back();
    	
    }

    // like check
    public function check_like($id)
    {
    	$like = Like::where('quote_id', $id)->where('user_id', Auth::id())->first();
    	if ($like == null) {
    		return true;
    	} else {
    		return false;
    	}
    }
}
